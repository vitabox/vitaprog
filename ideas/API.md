# API

# What can we ask?
- Whats the state of a compartment?
  -> id, picket at
- Whats the status of a compartment?
  -> id, light\_intensity, picked\_at, filled?

# What do we want to do?
- empty a compartment
- set the picket\_at of a compartment

# What are possible forms of API?
- Rest
- GraphQL
- COAP
