# Vitaprog

[![build status](https://gitlab.com/vitabox/vitaprog/badges/master/build.svg)](https://gitlab.com/vitabox/vitaprog/commits/master)
[![coverage report](https://gitlab.com/vitabox/vitaprog/badges/master/coverage.svg)](https://gitlab.com/vitabox/vitaprog/commits/master)

## What is it?
This is the program running on a VitaBox, or at least it will be.

## What does it do?
It will manage the compartments of a VitaBox and have a nice web interface avaliable to update all the needed data. It will also take care that the actual physical VitaBox does the right thing at the right time.

## How is the software sturctured?
The VitaProg is modular and layered. The basic layers are:
- Domain Model (Compartment representation, Cycling, Lightning calculation)
- Persistence Layer (Persist all the data to disk)
- Web Layer (give the user a nice interface to work with the software)
- Firmware Layer (comunicate to the physical VitaBox and set the lights and stuff)

## Run on your own machiene
You will neeed Elixir and Erlang installed.

Note that if you want to compile for the Pi from a Mac machiene you need to have the crosscompilers installed and the right $CROSSCOMPILE flag set. You can get them [here](http://www.welzels.de/blog/en/arm-cross-compiling-with-mac-os-x/).

1. Clone from GitLab
```
git clone git@gitlab.com:vitabox/vitaprog.git
```
2. Get all the the depedencies (after switching to the new directory)
```
mix deps.get
```
3. Complie the code
```
mix compile
```
4. Make and burn the firmware
```
cd apps/firmware && mix firmware && mix firmware.burn
```
