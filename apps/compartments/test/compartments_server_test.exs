defmodule Compartments.Compartment.CompartmentsServerTest do
  use ExUnit.Case, async: true

  alias Compartments.CompartmentsServer
  alias Compartments.Compartment

  setup do
    CompartmentsServer.set_all([])
  end

  @vaild_datetime Timex.to_datetime({{2016, 06, 06}, {2, 54, 32}}, "etc/Utc")


  test "state is persisted after the CompartmentsServer is killed" do
    compartment = %Compartment{id: 132}
    CompartmentsServer.insert_compartment(compartment)
    old_pid = GenServer.whereis(CompartmentsServer)
    GenServer.stop(old_pid)
    :timer.sleep 100
    new_pid = GenServer.whereis(CompartmentsServer)

    assert new_pid != nil
    assert new_pid != old_pid
    assert CompartmentsServer.get_all == [compartment]
  end

  test "getting a non existant compartment by id gives not found" do
    assert CompartmentsServer.get_by_id(100) === :not_found
  end

  test "getting a existant compartment by id" do
    compartment = %Compartment{id: 100}
    CompartmentsServer.insert_compartment(compartment)
    assert CompartmentsServer.get_by_id(100) === compartment
  end

  test "getting all compartment for no compartments" do
    assert CompartmentsServer.get_all() === []
  end

  test "getting all compartments" do
    compartment = %Compartment{}
    CompartmentsServer.insert_compartment(compartment)
    assert CompartmentsServer.get_all() === [compartment]
  end

  test "set all" do
    compartment = %Compartment{picked_at: @vaild_datetime}
    CompartmentsServer.set_all([compartment, compartment])
    assert CompartmentsServer.get_all() === [compartment, compartment]
  end

  test "set by id" do
    id = 123
    time1 = Timex.to_datetime({{2016, 06, 09}, {4, 50, 26}}, "etc/Utc")
    time2 = Timex.to_datetime({{2016, 06, 10}, {4, 50, 26}}, "etc/Utc")
    compartment1 = %Compartment{picked_at: time1, id: id}
    compartment2 = %Compartment{picked_at: time2, id: id}
    CompartmentsServer.insert_compartment(compartment1)
    CompartmentsServer.set_compartment(compartment2)
    assert CompartmentsServer.get_by_id(id) === compartment2
  end

  test "insert compartment" do
    compartment = %Compartment{picked_at: @vaild_datetime}
    CompartmentsServer.set_all([])
    CompartmentsServer.insert_compartment(compartment)
    assert CompartmentsServer.get_all() === [compartment]
  end
end
