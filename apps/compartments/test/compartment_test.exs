defmodule Compartments.ComparmentTest do
  use ExUnit.Case, async: true
  use Timex

  alias Compartments.Compartment

  test "Comparment struct constructor works when all field are given" do
    assert %Compartment{id: 10, picked_at: Timex.now()}
  end

  test "compartment sturct has sane defaults" do
    assert %Compartment{} === %Compartment{id: 0, picked_at: ~D[2017-01-01]}
  end


  test "is_empty is true for empty compartment" do
    compartment = %Compartment{picked_at: nil}
    assert Compartment.is_empty compartment
  end

  test "is_empty is false for non empty compartment" do
    compartment = %Compartment{picked_at: Timex.now()}
    refute Compartment.is_empty compartment
  end

  test "light is off for empty" do
    compartment = %Compartment{picked_at: nil}
    assert Compartment.light_intensity(compartment) == 0
  end

  test "light is on for non empty" do
    compartment = %Compartment{picked_at: Timex.now()}
    assert Compartment.light_intensity(compartment) !== 0
  end

end
