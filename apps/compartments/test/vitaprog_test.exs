defmodule CompartmentsTest do
  use ExUnit.Case
  doctest Compartments

  test "the truth" do
    assert 1 + 1 == 2
  end

  test "compartments supervisor starts in the beginning" do
    assert {:error, {:already_started, _pid}} = Compartments.start([], [])
  end
end
