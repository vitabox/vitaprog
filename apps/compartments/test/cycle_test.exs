defmodule Compartments.CycleTest do
  use ExUnit.Case, async: true

  alias Compartments.Cycle

  test "cycle is 0 in the beginning" do
    assert Cycle.light_intensity(0) == 0
  end

  test "cycle in 0 again after a cycle" do
    assert Cycle.light_intensity(3600) == 0
  end

  test "cycle maxes at half a cycle" do
    assert Cycle.light_intensity(1800) == 1
  end

  test "take_out_suggestion exists" do
    nums = [0, 100, 200, 300]
    suggestions = Enum.map nums, &Cycle.take_out_suggestion/1

    assert length(suggestions) == length(nums)
    assert is_number(Enum.sum(suggestions))
    assert Enum.all?(suggestions, &is_number/1)
  end
end
