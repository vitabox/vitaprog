defmodule Compartments do
  @moduledoc """
  This application manages compartments, the heart of the VitaBox. It keeps track of what is where.
  It also does all calculations based on them like the light intensity.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      # Starts a worker by calling: Compartments.Worker.start_link(arg1, arg2, arg3)
      # worker(Compartments.Worker, [arg1, arg2, arg3]),
      worker(Compartments.CompartmentsServer, [])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Compartments.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
