defmodule Compartments.Cycle do
  @moduledoc """
  All functions concering the cyclic values inside a VitaBox,
  e.g. the light intensitiy
  """

  @light_intensity_cycle_length 3600 # in seconds

  @doc """
  Takes passed time in seconds and gives back the current light intensity in percent
  """
  @spec light_intensity(number) :: float
  def light_intensity(time) do
    :math.sin(tau() * (time * (1 / @light_intensity_cycle_length) + 0.75)) * 0.5 + 0.5
  end

  @doc """
  Takes passed time and returns a suggestion on how good it would be to take out the food now
  """
  def take_out_suggestion(time) do
    # for now we just give back the light intesity value, because it's a nice cycle, with min = 0 and max = 1
    :math.sin(tau() * (time * (1 / @light_intensity_cycle_length) + 0.75)) * 0.5 + 0.5
  end


  defp tau, do: 2 * :math.pi
end
