defmodule Compartments.Compartment do
  @moduledoc """
  This module represents a compartment as a struct and keeps all related helper functions operating on a single compartment.
  """

  use Timex

  defstruct id: 0, picked_at: ~D[2017-01-01]

  @spec is_empty(%Compartments.Compartment{}) :: boolean
  def is_empty(compartment) do
    if compartment.picked_at, do: false,  else: true
  end

  @spec light_intensity(%Compartments.Compartment{}) :: number
  def light_intensity(compartment) do
    if compartment.picked_at do
      now = Timex.now()
      time_passed = Timex.diff(now, compartment.picked_at, :seconds)
      Compartments.Cycle.light_intensity(time_passed)
    else
      0.0
    end
  end
end
