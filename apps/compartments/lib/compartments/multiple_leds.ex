defmodule Compartments.MultipleLeds do
  def set(ids, picked_at) do
    Enum.each(ids, fn id ->
      compartment = %Compartments.Compartment{id: id, picked_at: picked_at}
      Compartments.CompartmentsServer.set_compartment(compartment)
    end)
  end
end
