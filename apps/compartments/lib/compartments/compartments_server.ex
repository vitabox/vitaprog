defmodule Compartments.CompartmentsServer do
  @moduledoc """
  This is a basic GenServer managing a collection of compartments.
  """

  use GenServer
  alias Compartments.Compartment

  @initial_state []
  @type state :: [%Compartment{}]

  # Client

  @doc "Start the CompartmentsServer"
  @spec start_link([any]) :: GenServer.on_start
  def start_link(args \\ []) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  @doc """
  Get the Compartment with a specific id
  """
  @spec get_by_id(integer) :: %Compartment{} | :not_found
  def get_by_id(id) do
    GenServer.call(__MODULE__, {:get_by_id, id})
  end


  @doc """
  Get all the Compartments in the collection as a list.

  Please not that ordering by ids is *not* guarenteed. The first element must not be the compartment with the id 1.
  """
  @spec get_all() :: [%Compartment{}]
  def get_all() do
    GenServer.call(__MODULE__, :get_all)
  end

  @doc """
  Set all compartments. This function replaces the entire collection with the given list
  """
  @spec set_all([%Compartment{}]) :: :ok
  def set_all(compartments) do
    GenServer.cast(__MODULE__, {:set_all, compartments})
  end

  @doc """
  Set a Compartment. This function takes a %Compartment{} updates it according to it's id.
  """
  @spec set_compartment(%Compartment{}) :: :ok
  def set_compartment(compartment) do
    GenServer.cast(__MODULE__, {:set_compartment, compartment})
  end

  @doc """
  Insert a new compartment into the collection.
  This makes no guarantees about the uniqueness of the id.
  """
  @spec insert_compartment(%Compartment{}) :: :ok
  def insert_compartment(compartment) do
    GenServer.cast(__MODULE__, {:insert_compartment, compartment})
  end

  # Callbacks

  @spec init(any) :: {:ok, []}
  def init(_args) do
    case Persistence.get_compartments() do
      nil -> {:ok, @initial_state}
      compartments -> {:ok, compartments}
    end
  end

  def handle_call(:get_all, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:get_by_id, id}, _from, state) do
   {:reply, get_compartment_by_id(state, id), state}
  end

  def handle_cast({:set_all, compartments}, _state) do
    update_compartments(compartments)
  end

  def handle_cast({:set_compartment, compartment}, state) do
    set_compartment_helper(state, compartment)
    |> update_compartments()
  end

  def handle_cast({:insert_compartment, compartment}, state) do
    update_compartments([compartment | state])
  end

  # Helpers

  defp update_compartments(compartments) do
    Persistence.put_compartments(compartments)
    {:noreply, compartments}
  end

  defp get_compartment_by_id(compartments, id) do
    Enum.reduce compartments, :not_found, fn(x, acc) ->
      if x.id === id, do: x, else: acc
    end
  end

  defp set_compartment_helper(compartments, new_compartment) do
    # Why not use a map? This version prevents duplicates from being inserted
    with_out_replacement = Enum.reject compartments, fn compartment -> compartment.id === new_compartment.id end
    [new_compartment | with_out_replacement]
  end
end
