# Persistence

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `persistence` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:persistence, "~> 0.1.0"}]
    end
    ```

  2. Ensure `persistence` is started before your application:

    ```elixir
    def application do
      [applications: [:persistence]]
    end
    ```

