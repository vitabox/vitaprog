defmodule PersistenceTest do
  use ExUnit.Case
  doctest Persistence

  @compartments [1,2,3]
  @storage_path "/tmp/vitabox_persistence_data_test"

 test "store and retrieve compartments" do
    Persistence.put_compartments @compartments
    assert File.exists? @storage_path <> "/compartments.storage"
    assert @compartments == Persistence.get_compartments
  end

end
