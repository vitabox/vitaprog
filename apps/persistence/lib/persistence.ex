defmodule Persistence do
  @moduledoc """
  This are the library functions for persisting data to disk
  """

  @doc """
  store the given compartments on disk
  """
  def put_compartments(compartments) do
    put(:compartments, compartments)
  end

  @doc """
  retrieve the compartments stored on disk
  """
  def get_compartments do
    get(:compartments)
  end

  def put(key, term) do
    PersistentStorage.put :settings, key, term
  end

  def get(key) do
    PersistentStorage.get :settings, key
  end
end
