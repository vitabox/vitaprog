defmodule Firmware.LedDriver do
  @moduledoc """
  LedDriver is a GenServer able talking to a SN3218.

  It exposes an easy way to use its I2C interface base on p. 6f of the datasheet.

  Note: the GenServer calls handle updating the driver them selves
  """
  use GenServer
  alias Firmware.LedDriver.I2cInterface

  @i2c_address 84
  @cmd_enable_output 0x00
  @cmd_set_pwm_values 0x01
  @cmd_enable_leds 0x13
  @cmd_update 0x16
  @cmd_reset 0x17

  # Client

  def start_link(default \\ []) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  @doc """
  Enable the power output to the LEDs
  if it is off, no LED receives power
  """
  def enable_output() do
    GenServer.cast(__MODULE__, :enable_output)
  end

  @doc """
  Disable the power output to the LEDs
  no LED receives power now
  """
  def disable_output() do
    GenServer.cast(__MODULE__, :disable_output)
  end

  @doc """
  Reset alle state of the LEDs
  """
  def reset() do
    GenServer.cast(__MODULE__, :reset)
  end

  @doc """
  set which LEDs are off, which are on
  Takes a list of booleans of length 18 representing the state of each led.

  E. G. [true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false] means only the first two are on, the others are off
  """
  def set_leds(enable_mask) do
    GenServer.cast(__MODULE__, {:set_leds, enable_mask})
  end

  @doc """
  Force the LEDDriver to do an update. You do need to call this function after making any change to the state, for your changes to take effect.
  """
  def update() do
    GenServer.cast(__MODULE__, :update)
  end

  @doc """
  Set the light intesity of a specific led.

  ## Arguments
  - location - the location of the led (1 to 18)
  - intensity - the light intensity the set should be set to (0 to 1)
  """
  def set_light_intensity(location, intensity) do
    GenServer.cast(__MODULE__, {:set_light_intensity, location, intensity})
  end
  # Server (Callbacks)

  def init(args \\ []) do
    response = I2cInterface.setup()
    case response do
      {:ok, i2c_pid} -> {:ok, %{i2c_pid: i2c_pid}}
      _ -> {:stop, :cant_start}
    end
  end

  def handle_cast(:enable_output, %{i2c_pid: i2c_pid} = state) do
    I2cInterface.enable_output(i2c_pid)
    {:noreply, state}
  end

  def handle_cast(:disable_output, %{i2c_pid: i2c_pid} = state) do
    I2cInterface.disable_output(i2c_pid)
    {:noreply, state}
  end

  def handle_cast(:reset, %{i2c_pid: i2c_pid} = state) do
    I2cInterface.reset(i2c_pid)
    {:noreply, state}
  end

  @doc """
  Toggels LEDs. enable_mask is a list of length 18 containing true and false to tell which LEDs to turn on.
  Example: The first two on, rest off: [true, true, false, false, ...]
  """
  def handle_cast({:set_leds, enable_mask}, %{i2c_pid: i2c_pid} = state) do
    enable_mask_padded = Enum.take(Enum.concat(enable_mask, List.duplicate(false, 18)), 18)

    I2cInterface.set_leds(i2c_pid, enable_mask_padded)
    {:noreply, state}
  end

  def handle_cast(:update, %{i2c_pid: i2c_pid} = state) do
    I2cInterface.update(i2c_pid)
    {:noreply, state}
  end

  def handle_cast({:set_light_intensity, location, intensity}, %{i2c_pid: i2c_pid} = state) do
    pwm_value = trunc(intensity * 256) # map the 0 to 1 intensity scale onto the 0 to 256 integer scale of the pwm
    I2cInterface.set_pwm(i2c_pid, location, pwm_value)
    {:noreply, state}
  end
end
