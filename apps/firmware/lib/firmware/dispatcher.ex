defmodule Firmware.Dispatcher do
  alias Firmware.LedDriver
  alias Compartments.CompartmentsServer
  alias Compartments.Compartment

  def start_link(default \\ []) do
    Task.start_link(fn -> loop() end)
  end

  def loop() do
    compartments = CompartmentsServer.get_all()
    update_leds(compartments)
    :timer.sleep(5000)
    loop()
  end

  def update_leds(compartments) do
    sorted_compartments = Enum.sort(compartments, &(&1.id <= &2.id))

    enable_mask = Enum.map(sorted_compartments, fn compartment ->
      Compartment.is_empty(compartment)
    end)

    LedDriver.set_leds(enable_mask)

    for compartment <- compartments do
      if (compartment.id < 18 && compartment.id > 0) do
        intensity = Compartment.light_intensity(compartment)
        LedDriver.set_light_intensity(compartment.id, intensity)
      end
    end

    LedDriver.update()
  end
end
