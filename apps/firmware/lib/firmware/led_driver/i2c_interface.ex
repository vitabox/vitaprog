defmodule Firmware.LedDriver.I2cInterface do
  @moduledoc """
  I2cInterface defines the I2c calls to be made to the SN3218.
  """

  @i2c_address 84
  @cmd_enable_output 0x00
  @cmd_set_pwm_values 0x01
  @led_control_register_one 0x13
  @led_control_register_two 0x14
  @led_control_register_three 0x15
  @cmd_update 0x16
  @cmd_reset 0x17

  def setup() do
    I2c.start_link("i2c-1", @i2c_address)
  end

  # Basic Interface options defined on p. 6f of the SN318 datasheet

  def enable_output(pid) do
    I2c.write(pid, <<@cmd_enable_output, 0x01>>)
  end

  def disable_output(pid) do
    I2c.write(pid, <<@cmd_enable_output, 0x00>>)
  end

  def reset(pid) do
    I2c.write(pid, <<@cmd_reset, 0x00>>)
  end

  def update(pid) do
    I2c.write(pid, <<@cmd_update, 0x00>>)
  end

  def set_pwm(pid, position, value) do
    I2c.write(pid, <<position, value>>)
  end

  def set_led_control_register_one(pid, enable_mask) do
    I2c.write(pid, <<@led_control_register_one>> <> enable_mask)
  end

  def set_led_control_register_two(pid, enable_mask) do
    I2c.write(pid, <<@led_control_register_two>> <> enable_mask)
  end

  def set_led_control_register_three(pid, enable_mask) do
    I2c.write(pid, <<@led_control_register_three>> <> enable_mask)
  end

  # Convenicene Functions
  @doc """
  set_leds takes (besides the i2c pid) a list of 18 booleans representing the desired state for each led.
  """
  def set_leds(pid, mask) do

    [first, second, thrid] =
      Enum.chunk_every(mask, 6)
      |> Enum.take(3)
      |> Enum.map(&booleans_to_numbers/1)
      |> Enum.map(&led_control_register_bits/1)

    set_led_control_register_one(pid, first)
    set_led_control_register_two(pid, second)
    set_led_control_register_three(pid, thrid)
  end

  defp led_control_register_bits([one, two, three, four, five, six]) do
    <<
      0 :: size(2),
      six :: size(1),
      five :: size(1),
      four :: size(1),
      three :: size(1),
      two :: size(1),
      one :: size(1)
    >>
  end

  defp booleans_to_numbers(xs), do: Enum.map(xs, &boolean_to_number/1)

  defp boolean_to_number(x) do
    if x do
      1
    else
      0
    end
  end
end
